package it.ru.matveev.alexey.jira.integrationtests.selenium;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExampleTest extends BaseJiraFuncTest {

    @Before
    public void setup() {
        backdoor.project().addProject("testproject", "TP", "admin");
        backdoor.issues().createIssue("TP", "test issue");
    }

    @Test
    public void goToIssueNavigator() {
        navigation.login("admin");
        navigation.issueNavigator().createSearch("project = TP");
        tester.assertTextPresent("TP-1");
    }

    @After
    public void cleanup() {
        backdoor.project().deleteProject("TP");
    }
}
