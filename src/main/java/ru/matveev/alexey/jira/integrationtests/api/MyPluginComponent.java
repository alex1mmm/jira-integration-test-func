package ru.matveev.alexey.jira.integrationtests.api;

public interface MyPluginComponent
{
    String getName();
}